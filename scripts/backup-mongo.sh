#!/bin/sh
set -ex

MONGO_USER="root"
MONGO_PASS="supersecretpass1234!"
FOLDER="/backups/`date +%Y-%m-%d`"
LOG_FILE="$FOLDER/log.txt"

echo "mkdir $FOLDER&& touch $LOG_FILE&& exec > $LOG_FILE 2>&1; mongodump -u $MONGO_USER -p $MONGO_PASS -o $FOLDER > $LOG_FILE " | docker exec -i flip-card-mongo-master sh
